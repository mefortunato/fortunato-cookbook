import Vue from 'vue'
import VueRouter from 'vue-router'

import Account from '@/components/Account'
import ListRecipes from '@/components/ListRecipes'
import Recipe from '@/components/Recipe'
import NotFound from '@/components/NotFound'

Vue.use(VueRouter)

const routes = [
  { path: '/', component: ListRecipes },
  { path: '/account', component: Account },
  { path: '/recipes', component: ListRecipes },
  { path: '/recipe/:id', component: Recipe },
  { path: '*', component: NotFound},
]

const router = new VueRouter({
  mode: 'history',
  routes: routes
})

export default router