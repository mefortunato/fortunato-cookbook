import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

// firebase init goes here
var config = {
    apiKey: "AIzaSyArGxYM7apVerawHdnZH4EOymT1r529rL8",
    authDomain: "fortunato-cookbook.firebaseapp.com",
    databaseURL: "https://fortunato-cookbook.firebaseio.com",
    projectId: "fortunato-cookbook",
    storageBucket: "fortunato-cookbook.appspot.com",
    messagingSenderId: "938070058791",
    appId: "1:938070058791:web:63284f9dc8664d672ceb02",
    measurementId: "G-2V7EBLWL7J"
  }
firebase.initializeApp(config)

// firebase utils
const db = firebase.firestore()
const auth = firebase.auth()
const storage = firebase.storage()
const currentUser = auth.currentUser

// firebase collections
const recipesCollection = db.collection('recipes')

export {
    db,
    auth,
    storage,
    currentUser,
    recipesCollection
}
