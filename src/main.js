import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'

import router from './router'
import store from './store'
const fb = require('./firebaseConfig.js')

Vue.config.productionTip = false

fb.auth.onAuthStateChanged(user => {
  store.dispatch("fetchUser", user);
})

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app')
